changes <- read.csv('dataset.csv')
projects <- sort(unique(changes$project))

#############################################################
# ELOC X SC plot
#############################################################
my.plot <- function() {
par(mfrow=c(length(projects), 1), mar = c(2.0,4.0,2.0,0.5))
for (proj in projects) {
  project_changes <- subset(changes, project == proj)
  project_changes <- project_changes[order(project_changes$normalized_date),]
  plot(
    project_changes$normalized_date,
    project_changes$total_eloc / max(project_changes$total_eloc),
    ylim = c(0,1),
    border = NA,
    type = 'l',
    col = 'gray',
    main = '',
    xlab = 'Project timeline',
    ylab = proj
    
  )
  par(new = TRUE)
  plot(
    project_changes$normalized_date,
    project_changes$structural_complexity / max(project_changes$structural_complexity),
    ylim = c(0,1),
    border = NA,
    type = 'l',
    col  = 'black',
    xlab = '',
    ylab = '',
    xaxt = 'n',
    yaxt = 'n'
  )
  legend('bottomright', col=c('gray', 'black'), lty = 'solid', legend=c('Lines of Code', 'Structural Complexity'))
}
dev.off()
}

WIDTH = 5
HEIGHT = 8

setEPS()
postscript(onefile = FALSE, file =  'plots/evolution.eps', height = HEIGHT, width = WIDTH)
my.plot()
cairo_pdf(filename = 'plots/evolution.pdf', height = HEIGHT, width = WIDTH)
my.plot()

#############################################################
warnings()

# vim: ft=r tw=0
