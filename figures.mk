EXTRA_DIST += figures.mk
FIGURES = $(wildcard figures/*.svg)
FIGURES_EPS = $(patsubst %.svg, %.eps, $(FIGURES))
FIGURES_EPS = $(patsubst %.svg, %.pdf, $(FIGURES))

OTHER += $(FIGURES_EPS)
CLEAN_FILES += $(FIGURES_EPS)

%.eps: %.svg
	inkscape --export-eps=$@ $<

%.pdf: %.svg
	inkscape --export-pdf=$@ $<
