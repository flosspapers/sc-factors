[default][duration=20]
-- [title] ------------------------------------------------------------
<b>Understanding Structural Complexity
Evolution: A Quantitative Analysis</b>




<small>
<b>Antonio Terceiro¹</b>, Manoel Mendonça¹,
Christina Chavez¹, Daniela Cruzes²

¹ Federal University of Bahia (UFBA), Brazil
² Norwegian University of Science and Technology
</small>

--
<emph>Maintenance</emph> consumes a large part
of software life cycle <emph>costs</emph>

-- [40percent.png][text-color=white][top]
Maintenance costs -- lower figures

-- [75percent.png][text-color=white][top]
Maintenance costs -- higher figures

--

<emph>Internal quality</emph> affects
maintenance costs

-- [wtfm.jpg]

--
Structural Complexity:
<emph>Coupling</emph> × Lack of <emph>Cohesion</emph>

<cite>
D. P. Darcy, C. F. Kemerer, S. A. Slaughter, and J. E. Tomayko,
<i>“The structural complexity of software: An experimental test,”</i> IEEE
Transactions on Software Engineering, vol. 31, no. 11, pp. 982–995,
Nov. 2005.
</cite>


--
↑ Structural Complexity
<emph>↑ Maintenance Effort</emph>

<cite>
D. P. Darcy, C. F. Kemerer, S. A. Slaughter, and J. E. Tomayko,
<i>“The structural complexity of software: An experimental test,”</i> IEEE
Transactions on Software Engineering, vol. 31, no. 11, pp. 982–995,
Nov. 2005.
</cite>

--
↑ Structural Complexity
<emph>↓ New developers</emph>

<small>(in FLOSS projects)</small>

<cite>
P. Meirelles, C. S. Jr., J. Miranda, F. Kon, A. Terceiro, and C. Chavez, “A
study of the relationships between source code metrics and attractiveness
in free software projects,” in CBSOFT-SBES2010. Los Alamitos, CA,
USA: IEEE Computer Society, sep 2010, pp. 11–20.
</cite>


--
<emph>Which factors</emph> cause the
Structural Complexity to change?

-- [experience-of-developers.png][top]
The <emph>experience</emph> of developers in the project

-- [change-diffusion.png][top]
The <emph>change diffusion</emph> of the changes
made to the system

-- [size-variation.png][top]
The <emph>size variation</emph> caused by changes
made to the system

--
<emph>Goals of this study</emph>

Investigate the influence of <emph>Developer Experience on
the project</emph>, <emph>Change Diffusion</emph> and <emph>Size Variation</emph> over
the <emph>Structural Complexity variation</emph>


-- [title] ------------------------------------------------------------
<h>Experimental setup</h>

--
Unit of analysis: commit

--
<h>Independent variables</h>

<math>n</math> – experience
<math>CF</math> – change diffusion
<math>ΔLOC</math> – size variation

--
<h>Dependent variables</h>

<math>ΔSC</math><sub><math>i</math></sub> – increase in SC
<math>ΔSC</math><sub><math>d</math></sub> – decrease in SC

-- [sc.png]

-- [variables-001.png]
#
-- [variables-002.png]
#
-- [variables-003.png]
#
-- [variables-004.png]
#
-- [variables-005.png]
#
-- [variables-006.png]
#
-- [variables-007.png]
#
-- [variables-008.png]

-- [sample.png]

-- [evolution-node+voldemort.png]

-- [title] ------------------------------------------------------------
<h>Results</h>

-- [linear-model.png]

-- [results.png]

-- [results-experience.png]

-- [results-sizevar.png]

-- [results-diffusion.png]

-- [title] ------------------------------------------------------------
<h>Conclusions</h>

-- [factors.png][top-left]
<emph>All factors</emph> were influent in
<emph>at least 2 cases</emph>

-- [factors.png][top-left]
<emph>Change diffusion</emph> was the
<emph>most influential</emph> factor

-- [factors.png][top-left]
System <emph>size variation</emph> is
<emph>not necessarily associated</emph> with
structural complexity variation

-- [factors.png][top-left]
<emph>Different projects</emph> are influenced by
<emph>different factors</emph>

-- [factors.png][top-left]
<emph>Increase and decrease</emph> in structural
complexity are influenced by
<emph>different sets of factors</emph>

-- [factors-r2.png][top-left]
There are <emph>other factors</emph> that were
<emph>not explored</emph> in this study

-- [title] ------------------------------------------------------------
<h>Future work</h>

--
Other factors such as <emph>familiarity</emph>
of the developers with the modules being changed and
<emph>type of maintenance</emph>.
--
<emph>Qualitative studies</emph> on the commits

-- [title] ------------------------------------------------------------
Thank you

Antonio Terceiro
<h>terceiro@dcc.ufba.br</h>
@aterceiro
