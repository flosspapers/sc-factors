commits <- read.csv('dataset.csv')
commits$core <- sapply(commits$core, function(c) { if (c == 't') { 1 } else { 0 } })

projects <- as.character(unique(commits$project))
for (p in projects) {

  assign(p, subset(commits, project == p))

  assign(paste(p, 'inc', sep = '.'), subset(get(p), delta_structural_complexity > 0))
  assign(paste(p, 'dec', sep = '.'), subset(get(p), delta_structural_complexity < 0))
  assign(paste(p, 'keep', sep = '.'), subset(get(p), delta_structural_complexity == 0))

}

banner <- function(msg) {
  cat("**************************************************\n")
  cat("* ", msg, "\n", sep = '')
  cat("**************************************************\n")
}

project.subset <- function(p, suffix) {
  get(paste(p, suffix, sep = '.'))
}

change.type <- data.frame(project = projects)
change.type$keep  <- 100 * sapply(projects, function(p) { nrow(project.subset(p,'keep'))/nrow(get(p)) })
change.type$inc   <- 100 * sapply(projects, function(p) { nrow(project.subset(p,'inc'))/nrow(get(p)) })
change.type$dec   <- 100 * sapply(projects, function(p) { nrow(project.subset(p,'dec'))/nrow(get(p)) })
