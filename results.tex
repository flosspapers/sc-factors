

An initial interesting fact that resulted from our data analysis was
that the different ways of measuring experience within a project we
propose in this paper are practically equivalent. Table
\ref{table:experience-measures-correlation} shows, for each project, the
Spearman's $\rho$ correlation between $n$ (number of commits done by the
developer so far) and $d$ (number of days since the developer started in
the project). One can see that there is a very strong linear correlation
between these variables, what means we only need to look at one of them.
For the rest of this study, we will only present and discuss the
hypotheses using $n$ as independent variable: their results are similar
to the ones where we used $d$ as independent variable.

\rowcolors{3}{lightgray}{white}
\begin{table}[hbt]
  \centering
  \caption{Linear correlations between $n$ and $d$}
  \label{table:experience-measures-correlation}
  \vspace{0.25em}
  % TODO do not hardcode the data like this!
  \begin{tabular}{llr}
    \hline
    Project               & Commits     & $\rho(n,d)$ \\
    \hline
    clojure               & 522         & 0.996       \\
    node                  & 240         & 0.961       \\
    redis                 & 117         & 0.984       \\
    voldemort             & 394         & 0.875       \\
    zeromq2               & 128         & 0.884       \\
    \hline
  \end{tabular}
  \\\vspace{0.25em}\small{All correlations statistically significant with $p < 0.001$}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Commits that increase structural complexity
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\rowcolors{3}{lightgray}{white}
\begin{table*}
  \centering
  \caption{Regression models for commits that \textbf{increase} structural
  complexity}
% latex table generated in R 2.14.1 by xtable 1.5-6 package
% Tue Jan 17 11:47:54 2012
\begin{tabular}{lrllllr}
  \hline
Project & \#Commits & Intercept & $n$ ($H_1$) & $\Delta{}LOC$ ($H_2$) & CF ($H_3$) & Adj. $R^2$ \\ 
  \hline
clojure & 287 & 0.031637  & 0.000108  & \textbf{0.000147 ***} & \textbf{0.014175 ***} & 0.20 \\ 
  node & 133 & \textbf{0.340228 ***} & \textbf{-0.000479 **} & 0.000369  & \textbf{0.025465 *} & 0.10 \\ 
  redis &  72 & -1.848899  & 0.004697  & 0.00244  & \textbf{0.236758 ***} & 0.31 \\ 
  voldemort & 217 & 0.026552  & -5e-06  & \textbf{0.000195 **} & \textbf{0.002326 ***} & 0.22 \\ 
  zeromq2 &  71 & \textbf{0.224476 **} & -0.000399  & \textbf{0.000746 **} & 0.004965  & 0.21 \\ 
   \hline
\end{tabular}\\
\vspace{0.5em}
***: $p < 0.001$; **: $p < 0.01$; *: $p < 0.05$
  \label{tab:models:inc}
\end{table*}

To test hypotheses $H_1$ to $H_3$, we built regression models in which
the structural complexity increase is modeled as a linear combination
of the experience of the developer making that change, the size
variation and the change diffusion:

\[
\Delta{}SC_i =
  \alpha_0 +
  \alpha_1  n +
  \alpha_2  \Delta{}LOC +
  \alpha_3  CF
\]

Table \ref{tab:models:inc} presents the resulting models for the commits
that increase structural complexity.

We can see that $H_1$ (developer experience influences the increase in
structural complexity negatively) holds only for Node, which is the only
project in which the experience variable $n$ has a statistically
significant coefficient with a negative sign. Our interpretation is that
the changes made by less experienced developers introduce more
structural complexity in the project codebase, or alternatively that
more experienced developers introduce less structural complexity.
%
Even though that coefficient seems to have a low magnitude,
one must note that the measures are in different scales.

$H_2$ (size variation influences the increase in structural complexity
positively) holds for Clojure, Voldemort and Zeromq2. In all three
projects, changes that introduce more lines of code also introduce more
structural complexity. The same observation about the magnitude of the
coefficient applies here: \dLOC~ and \dSCi~ are measured in different
scales.

Last, we have found that $H_3$ (change diffusion influences the increase
in structural complexity positively) holds for all projects, except
Zeromq2.  In these projects changes that touch more files tend to
introduce more structural complexity.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Commits that decrease structural complexity
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\rowcolors{3}{lightgray}{white}
\begin{table*}
  \centering
  \caption{Regression models for commits that \textbf{decrease} structural
  complexity}
% latex table generated in R 2.14.1 by xtable 1.5-6 package
% Tue Jan 17 11:47:54 2012
\begin{tabular}{lrllllr}
  \hline
Project & \#Commits & Intercept & $n$ ($H_4$) & $\Delta{}LOC$ ($H_5$) & CF ($H_6$) & Adj. $R^2$ \\ 
  \hline
clojure & 235 & -0.064833  & \textbf{0.000422 **} & 0.000204  & 0.011493  & 0.04 \\ 
  node & 107 & \textbf{0.313235 *} & -0.00038  & -0.000308  & \textbf{0.036385 **} & 0.10 \\ 
  redis &  45 & -0.359334  & 0.000952  & 0.000443  & \textbf{0.163235 **} & 0.18 \\ 
  voldemort & 177 & \textbf{0.029518 *} & 2.3e-05  & \textbf{0.00013 **} & 0.001125  & 0.07 \\ 
  zeromq2 &  57 & 0.10271  & -0.000372  & \textbf{-0.001258 ***} & \textbf{0.021416 ***} & 0.88 \\ 
   \hline
\end{tabular}\\
\vspace{0.5em}
***: $p < 0.001$; **: $p < 0.01$; *: $p < 0.05$
  \label{tab:models:dec}
\end{table*}

To test our hypotheses $H_4$ to $H_6$, we have built linear regression
models similar to the ones presented above. For each project, we
modeled the decrease in structural complexity as a linear combination
of experience of the developer, size variation and change diffusion:

\[
\Delta{}SC_d =
  \alpha_0 +
  \alpha_1  n +
  \alpha_2  \Delta{}LOC +
  \alpha_3  CF
\]

The results provided by these regression models are presented in table
\ref{tab:models:dec}.

$H_4$ (developer experience influences the decrease in structural
complexity positively) holds only for Clojure.  Our interpretation is
that in Clojure, the changes made by developers with more experience are
the ones that cause larger decreases in structural complexity. In that
project, anti-regressive work is done by the most experienced
developers.

$H_5$ (size variation influences the decrease in structural complexity
negatively) holds only for Zeromq2. In that project, the commits that
produce larger decreases in structural complexity are the ones with
lower number of lines of code added, or commits that actually remove
lines of code.
%
Voldemort also has a significant coefficient for \dLOC, but that
coefficient is negative: in that project commits that add more lines of
code are the ones producing larger decreases in structural complexity.
This sounds counter-intuitive: maybe the Voldemort developers were able
to reduce complexity by splitting existing modules into new ones that
are larger but less coupled and/or more cohesive.

$H_6$ (change diffusion influences the decrease in structural complexity
positively) holds for Node, Redis and Zeromq2. Our interpretation is
that in those projects the commits that touch more files are the ones
that cause a greater reduction in structural complexity.
