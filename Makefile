NAME = sc-factors
TEXSRCS = results.tex
BIBTEXSRCS = references.bib
OTHER += plots.stamp figures/variables.eps plots/
SUBMISSION = csmr2012-terceiro.pdf
CLEAN_FILES += plots/*.eps $(SUBMISSION)
VIEWPDF = evince
XDVI = evince

-include figures.mk

include /usr/share/latex-mk/latex.gmk

submission: $(SUBMISSION)

$(SUBMISSION):
	$(MAKE) clean
	USE_PDFLATEX=1 $(MAKE) pdf
	cp $(NAME).pdf $@

figures.mk:
	cp ~/research/figures.mk .

%.stamp: %.R dataset.csv
	Rscript $<
	touch $@

results.tex: results.Rnw analysis.R load.R
	R CMD Sweave results.Rnw || (rm -f $@; false)

references.bib:
	cp ~terceiro/research/bibliography.bib $@

yes-upload:
	git config hooks.skipupload false

no-upload:
	git config hooks.skipupload true

todo:
	ack-grep '\\TODO{' $(NAME).tex

clean::
	$(RM) *.ltx *.stamp developer_mapping.txt
