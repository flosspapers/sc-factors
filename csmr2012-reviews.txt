> vim: ft=mail
>
> Dear Antonio,
>
> Congratulations !
>
> We are pleased to inform you that your paper entitled "Understanding
> Structural Complexity Evolution: a Quantitative Analysis" has been
> accepted for inclusion as full paper in the technical program of
> CSMR'12, the 16th European Conference on Software Maintenance and
> Reengineering !
> [...]
>
> ----------------------- REVIEW 1 ---------------------
> PAPER: 87
> TITLE: Understanding Structural Complexity Evolution: a Quantitative Analysis
> AUTHORS: Antonio Terceiro, Manoel Mendonça, Christina Chavez and Daniela Cruzes
>
> Summary
>
> For each commit to a project the complexity was measured, alongside
> the corresponding change in size, the number of files changed and the
> developer's experience in the project. These measurements were used to
> build a linear regression model to identify the relative contribution
> of the 3 factors to structural complexity. Such models were built for
> 5 OSS projects. The study shows that CF (the number of changed files
> in the commit) was the more significantly associated factor for
> complexity changes, followed by changes in LOC and lastly developer
> experience.

OK

> Review
>
> The definition of structural complexity 'SC' (section II.A) based on
> Darcy et al. (2005) is a neat formalisation but seems to be applied in
> a wider role than intended. Darcy's work [6] refers to the importance
> of coupling and cohesion regarding perfective maintenance effort, yet
> SC is being used here for measurements concerning all varieties of
> code change. This is acknowledged in the 'threats to validity'
> (section VI) but including a methodology to identify commits related
> to perfective effort would strengthen the paper.

OK

  - unfortunately this is not possible ATM
  - mentioned as future work: categorize commits by type of maintenance

> It's refreshing that the measurement of developer experience isn't
> based on subjective metrics but on quantitative data. However it
> assumes that expertise with the project cannot decline due to periods
> of inactivity. A stronger methodology would consider the developers
> familiarity with the code at the point of the commit and whether they
> had a strong association with the code being changed. Some use of SNA
> and the methods shown by (for example) Zhang et al. (2011) to connect
> developers to modules they are familiar with may be useful.

OK

  - mentioned explicitly in threats to validity
  - mentioned in future work
  - look at that paper and perhaps include it as a reference for next
    paper

> That different projects showed different influencing factors is useful
> and serves as a reminder to avoid seeking a model covering all
> projects. The discussion as to why there are differences seems at
> points confused. The argument that “experience makes a difference in
> projects with large teams. ” is debatable – surely the relative
> contribution of experience will be felt most keenly in smaller teams?
> The argument that “...experience only plays a significant role in
> specific applications domains ...” is defeated by the authors data
> since if this were true it should show in the results for projects in
> the database domain too and not just those in the compiler domain.

OK

  - clarified those points

> While acknowledging the bias towards infrastructure software and not
> projects targeted at end users there is no rationale given for that
> choice. Is this because such infrastructure project exhibit a single
> evolution lineage as opposed to the typical branching and bundling
> nature of end user projects? i.e is the bias one of practicality?

OK

> The authors recommend that managers should assess factors such as
> experience when assigning developers for maintenance tasks, but there
> is no indication that this was already the practice for the OSS
> projects studied. If so how would this affect the results?

TODO

> The largest critique is also raised by the authors, that the R2 are
> low implying there are other factors influencing complexity and that,
> potentially, the 3 factors used are irrelevant. If so then the authors
> have measured 'noise' or second order effects making the paper's
> conclusions moot.

OK

> Figure 2 shows in all projects some large abrupt increases/decreases
> in complexity with minor changes in system size. The authors should
> have looked at the change log or at the commit messages to try to
> understand what explains those complexity changes to get deeper
> insights into structural complexity factors.

OK

  - mentioned future work: look at the top N commits based on |ΔSC|

> Perhaps a further null hypothesis could also have been usefully
> tested. The discarded commits (with ΔSC=0) should logically show their
> own correlation with the measured factors.

OK

  - no, because the dependent variable ΔSC has the same value (0) for all
    the points!

> Overall the paper is readable and the methodology clear and
> replicable. The hypotheses are well constructed and directly addressed
> by the collected data.

OK

> Contribution
>
> The paper demonstrates there are measurable factors that can increase
> or decrease the structural complexity of a project and that these can
> vary between projects. But without identifying the significant factors
> the contribution is limited.

OK

> Typos
>
> Section I : change diffusion may have some influence over *influence*
> structural complexity evolution Section III.B: the addition of new
> features and the correction of *deffects* Section V: evolution curve
> presents no *similar* whatsoever with the corresponding size evolution
> curve.

OK

> References
>
> Zhang, W.; Yang, Y. & Wang, Q. (2011) "Network analysis of OSS
> evolution: an empirical study on ArgoUML project", Proceedings of the
> 12th international workshop and the 7th annual ERCIM workshop on
> Principles on software evolution and software evolution, pp.71-80.
>
>
> ----------------------- REVIEW 2 ---------------------
> PAPER: 87
> TITLE: Understanding Structural Complexity Evolution: a Quantitative Analysis
> AUTHORS: Antonio Terceiro, Manoel Mendonça, Christina Chavez and Daniela Cruzes
>
> Summary:
>
> Empirically evaluated which one of code size, develper's experience, or
> change diffusion tends to increase/decrease structural complexity of products.
> From each revision of five repositores of open-source projects, collected
> data of code size (LOC), developer's experience (how long a developer joins
> the proejct), change diffusion (how spread the changes at the revision).
> Developed regression models between delta of structural complexity and
> these factors.
>
> Although the result seems preliminary one, the authors concluded that
> "change diffusion is most influential factor".
>
> Evaluation:
>
> Interesting research question.
> Understanding evolution is a promising topic in software engineering.
>
> Excellent experimental setup and the preliminary but intersting finding
> that change diffusion is more important factor of structural complexity
> than code size.

OK

> ----------------------- REVIEW 3 ---------------------
> PAPER: 87
> TITLE: Understanding Structural Complexity Evolution: a Quantitative Analysis
> AUTHORS: Antonio Terceiro, Manoel Mendonça, Christina Chavez and Daniela Cruzes
>
> The study investigates the correlation of 3 factors with structural
> complexity: experience of the developer, size of the commits, and
> change diffusion. The study is performed on 5 open-source system with
> a significant history. The paper reports on regression models built
> from the metrics; six hypotheses are investigated, as positive and
> negative correlations are investigated separately. However, these
> models mostly have somewhat weak explaining power (ranging from 0.04
> to 0.88, with the overwhelming majority at or below 0.3), meaning that
> other factors are at play. Of the factors considered, it seems that
> change diffusion is the one with the most influence.

OK

> This article investigates quite a lot of factors at once. I like that
> the paper distinguishes between increases and decreases of complexity
> (although on first read it was not obvious why, perhaps it should be
> stated more clearly, for instance in the abstract, that the two
> activities are distinct and hence treated distinctly).

OK

> I have one main worry about the paper however: the premise is to
> evaluate the correlation of various factors with structural
> complexity; however, inspecting the graphs of figure 2, one sees
> extremely large variations on structural complexity. For instance, the
> project "redis" sees a very sharp increase at t=0.7, followed by a
> decrease to its previous values, and then increases again to the level
> it had before.  Similarly, project "zeromq2" experiences large
> oscillations in complexity the last third of its life; projects "node"
> and "clojure" have sharp drops and rises as well. This is not
> explained in the paper. I would have expected, upon seeing this, an
> investigation of the causes of these changes, and a summary of these
> in the paper.  Without that, I am unsure on how to interpret the
> results. I wish this was clearly explained in the paper.

OK - mentioned as future work

> I also have a few minor comments:
>
> - "Defect" is consistently misspelled as "deffect"

OK

> - In the related work on change diffusion, there is recent paper by
>   Hassan ("Predicting faults using the complexity of code changes"),
>   that could be discussed.

OK

  - look at this paper
  - discuss it in the background section

> - Why is it specifically mentioned that the systems under study are
>   "infrastructure software"? This is not justified.

OK

> - The adjusted R2 is really low most of the time, expect when it's
>   not! It is extremely surprising to see it jumping from 0.04 to 0.88.
>   In that context, I found the explanations to be lacking (one sentence
>   is way to little; with such a difference, I would expect a detailed
>   investigation of the complexity-decreasing commits and the code base).

OK

> - For H5, given the counter-intuitive finding of size increases that
>   actually decrease complexity, I would also expect more investigation,
>   especially since the finding is at odds with the previous one. Again,
>   examples of commits would be helpful to understand what really is
>   going on.

OK - mentioned in the future work part
